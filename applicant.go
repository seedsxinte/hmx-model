package model

import (
	"net/mail"
	"time"
)

// appllicant info
type Applicant struct {
	Id         string       `form:"id" json:"id" binding:"required"`
	Birthday   time.Time    `form:"birthday" json:"birthday" binding:"required"`
	Gender     GenderType   `form:"gender" json:"gender"`
	Email      mail.Address `form:"email" json:"email" binding:"required"`
	Tel        []int        `form:"tel" json:"tel"`
	Name       []string     `form:"name" json:"name" binding:"required"`
	Kana       []string     `form:"kana" json:"kana" binding:"required"`
	Post       int          `form:"post" json:"post" binding:"required"`
	Prefecture string       `form:"prefecture" json:"prefecture" binding:"required"`
	City       string       `form:"city" json:"city" binding:"required"`
	Street     string       `form:"street" json:"street" binding:"required"`
	Optional   ApplicantSub `form:"optional" json:"optional"`
}

type ApplicantSub struct {
	EmailSub mail.Address `form:"emailSub" json:"emailSub"`
	Station  string       `form:"station" json:"station"`
	Married  bool         `form:"married" json:"married"`
	TelSub   []int        `form:"telSub" json:"telSub"`
}

type GenderType int

// GenderType constants via ISO-5218
const (
	NotKnown GenderType = 0 + iota
	Male
	Female
	NotApplicable
)

// GenderType stringer
func (i GenderType) String() string {
	switch i {
	case NotKnown:
		return "NotKnown"
	case Male:
		return "Male"
	case Female:
		return "Female"
	case NotApplicable:
		return "NotApplicable"
	}
	return "NotSelected"
}

// job apply info
type Apply struct {
	Id      string    `form:"id" json:"id" binding:"required"`
	Created time.Time `form:"created" json:"created" binding:"required"`
	Updated time.Time `form:"updated" json:"updated" binding:"required"`
}

// job skill
type Skill struct {
	Id string `form:"id" json:"id" binding:"required"`
}

// applicant note
type Note struct {
	Id   string   `form:"id" json:"id" binding:"required"`
	Note []string `form:note json:"note"`
}
