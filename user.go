package model

// login info
type Login struct {
	Id       string `form:"id" json:"id" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

// user for gateway
type User4UI struct {
	Id          int      `json:"id"`
	RoleId      int      `json:"roleId"`
	Name        string   `json:"name"`
	Email       string   `json:"email"`
	InfoRoomIds []RoomId `json:"infoRoomIds"`
}

// user for authentication
type User struct {
	Id              int `db:"id"`
	RoleId          int `db:"role_id"`
	Name            string
	Email           string
	CryptedPassword string `db:"crypted_password"`
	Salt            string
}
