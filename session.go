package model

// user auth success: true, fail: false
type Authenticated bool

// session model
type Session struct {
	Authenticated Authenticated `json:"authenticated"`
	User          User4UI       `json:"user"`
	Layout        Layout        `json:"layout"`
	Info          Info          `json:"info"`
}
