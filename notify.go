package model

// info id
type InfoId string

// info type id
type InfoType int

// room id
type RoomId string

// notification
type Notify struct {
	Infos       []Info   `json:"infos"`
	Dones       []InfoId `json:"dones"`
	InfoRoomIds []RoomId `json:"infoRoomIds"`
}

// info
type Info struct {
	InfoId    InfoId `json:"infoId"`
	Type      string `json:"type"`
	TimeStamp int64  `json:"timeStamp"`
}

// InfoType constants
const (
	InProcess InfoType = 1 + iota
	TodayScreening
	UnreadMessage
	UnreadErrorMessage
	PendingApproval
	Remanded
)

// InfoType stringer
func (i InfoType) String() string {
	switch i {
	case InProcess:
		return "InProcess"
	case TodayScreening:
		return "TodayScreening"
	case UnreadMessage:
		return "UnreadMessage"
	case UnreadErrorMessage:
		return "UnreadErrorMessage"
	case PendingApproval:
		return "PendingApproval"
	case Remanded:
		return "Remanded"
	}
	return "No InfoType"
}
