package model

// layout total
type Layout struct {
	ApplicantForm []FormGroup `json:"applicantForm"`
	Top           []Top       `json:"top"`
}

// top page
type Top struct {
	Caption  string `json:"caption"`
	Img      string `json:"img"`
	IsLink   bool   `json:"isLink"`
	Href     string `json:"href"`
	Children []Top  `json:"children"`
}

// form page
type FormGroup struct {
	Caption string  `json:"caption"`
	Inputs  []Input `json:"inputs"`
}

// input
type Input struct {
	Label    string   `json:"label"`
	Required bool     `json:"required"`
	Type     string   `json:"type"`
	Name     string   `json:"name"`
	Options  []Option `json:"options"`
}

// option
type Option struct {
	Label string `json:"label"`
	Value string `json:"value"`
}
